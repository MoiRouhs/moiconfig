#!/bin/bash

Main() {
    source /usr/share/endeavouros/scripts/eos-script-lib-yad || return 1
    tamanox=300
    tamanoy=200
    xaxis=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1)
    posicionx=$((1366 - $tamanox - 15)) 
    local command=(
        eos_yad 
          --calendar 
          --title="Fecha" 
          --no-buttons 
          --height=$tamanoy
          --width=$tamanox
          --posx=$posicionx
          --posy=30
          --show-weeks
          --text=`date +%H":"%M":"%S`
      )

    "${command[@]}"
}

Main "$@"
