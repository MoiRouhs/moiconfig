#!/bin/bash
distribucion=`setxkbmap -print | grep xkb_symbols | awk '{print $4}' | awk -F"+" '{print $2}'`
if [ "$distribucion" = "latam" ]; then
  exec setxkbmap es
  exit 
elif [ "$distribucion" = "es" ]; then
  exec setxkbmap us
  exit 
elif [ "$distribucion" = "us" ]; then
  exec setxkbmap latam
  exit 
fi
