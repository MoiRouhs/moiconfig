call plug#begin()
" Sintaxis
Plug 'sheerun/vim-polyglot'

"Temas
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Tree
Plug 'scrooloose/nerdtree'

" Autocompleta
Plug 'https://github.com/MoiRouhs/AutoComplPop.git'

" typing
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-surround'

" IDE
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'yggdroot/indentline'

" Syntax
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'yggdroot/indentline'

" Livecoding
Plug 'tpope/vim-fireplace'

" Vue
Plug 'leafOfTree/vim-vue-plugin'

" Art Nannou
Plug 'rust-lang/rust.vim'

" For React 
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }
Plug 'jparise/vim-graphql'

" Emmet
Plug 'mattn/emmet-vim'
call plug#end()



