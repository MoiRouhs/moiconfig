" AutoComplPop-master
set formatoptions-=tc
set cindent
set bs=2

" nerdtree
let NERDTreeShowHidden=0
let NERDTreeQuitOnOpen=0
let NERDTreeAutoDeleteBuffer=1
let NERDTreeMinimalUI=0
let NERDTreeDirArrows=1
let NERDTreeShowLineNumbers=1
let NERDTreeMapOpenInTab='\t'
autocmd BufEnter NERD_tree_* | execute 'normal R'
au CursorHold * if exists("t:NerdTreeBufName") | call <SNR>15_refreshRoot() | endif
augroup DIRCHANGE
  au!
  autocmd DirChanged global :NERDTreeCWD
augroup END

" IndentLine
let g:indentLine_char = 'c'
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
let g:indentLine_defaultGroup = 'SpecialKey'
let g:indentLine_color_term = 202
let g:indentLine_color_gui ='#f50057' 
let g:indentLine_color_tty_light = 7 " (default: 4)
let g:indentLine_color_dark = 1 " (default: 2)

" Emmet 
""set filetype:javascript.jsx
""let g:user_emmet_install_global = 0
""autocmd FileType html,css EmmetInstall
