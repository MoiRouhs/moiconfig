" tecla leader
let mapleader = ","

" Cerrar corchetes
inoremap { {}<Esc>ha
inoremap ( ()<Esc>ha
inoremap [ []<Esc>ha
inoremap " ""<Esc>ha
inoremap ' ''<Esc>ha
inoremap ` ``<Esc>ha

" Atajos de teclado
nmap <F2> <ESC>:setlocal spell spelllang=es<CR>
nmap <F3> <ESC>:setlocal spell spelllang=fr<CR>
nmap <F4> <ESC>:setlocal spell spelllang=en<CR>

" NREDTree
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <leader>r :NERDTreeFind<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
